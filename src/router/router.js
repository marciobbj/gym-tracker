import Vue from "vue";
import Router from "vue-router";

import Login from "./../components/Login";
import Home from "./../components/Home";
import Series from "./../components/Series";

Vue.use(Router);

const routes = [
	{
		path: "/",
		name: "Login",
		component: Login,
		meta: { title: "Login", menu: false }
	},
	{
		path: "/area",
		name: "Home",
		component: Home,
		meta: { title: "Home", menu: true }
	},
	{
		path: "/series",
		name: "Series",
		component: Series,
		meta: { title: "Series", menu: true }
	}
];
const router = new Router({
	routes,
	mode: "hash"
});

export default router;
