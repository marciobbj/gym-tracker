import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import VueRouter from "vue-router";
import Login from "./components/Login";
import Home from "./components/Home";
import Series from "./components/Series";
import Animate from "animate.css";

Vue.use(VueRouter);
Vue.use(Animate);

Vue.config.productionTip = false;

const routes = [
	{
		path: "/",
		name: "Login",
		component: Login,
		meta: { title: "Login", menu: false }
	},
	{
		path: "/area",
		name: "Home",
		component: Home,
		meta: { title: "Home", menu: true }
	},
	{
		path: "/series",
		name: "Series",
		component: Series,
		meta: { title: "Series", menu: true }
	}
];
const router = new VueRouter({
	routes
});
new Vue({
	render: (h) => h(App),
	router
}).$mount("#app");
