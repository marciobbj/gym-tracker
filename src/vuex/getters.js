const payload = (state) => ({
	currentUser: state.user ? state.user.name : null,
	exercices: null,
	place: null
});

export default {
	payload
};
