const currentUser = (state) => state.currentUser;
const userId = (state) => state.userId;

export default {
	currentUser,
	userId
};
