import axios from "axios";

const headers = {
	Accept: "application/json",
	"Content-type": "application/json"
};

const url = "http://127.0.0.1:8000";

const obtainToken = (username, password) => {
	const payload = {
		username,
		password
	};
	axios
		.post(this.state.endpoints.obtainJWT, payload)
		.then((response) => {
			this.commit("updateToken", response.data.token);
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
};
const refreshToken = () => {
	const payload = {
		token: this.state.jwt
	};
	axios
		.post(this.state.endpoints.refreshJWT, payload)
		.then((response) => {
			this.commit("updateToken", response.data.token);
		})
		.catch((error) => {
			console.error(error);
			throw error;
		});
};
const inspectToken = () => {
	const token = this.state.jwt;
	if (token) {
		const decoded = jwt_decode(token);
		const exp = decoded.exp;
		const orig_iat = decode.orig_iat;
		if (
			exp - Date.now() / 1000 < 1800 &&
			Date.now() / 1000 - orig_iat < 628200
		) {
			this.dispatch("refreshToken");
		} else if (exp - Date.now() / 1000 < 1800) {
		} else {
			//use this to dispatch logout
		}
	}
};
const getUser = (context, userId) =>
	axios
		.get(`${url}/api/v1/users/${userId}`)
		.then((response) => {
			context.commit("UPDATE_CURRENT_USER", response.data);
		})
		.catch((error) => {
			console.error(error, "Error getting user");
			throw error;
		});

const loginUser = (context, credentials) =>
	axios.post(`${url}/api-auth/login/`);
