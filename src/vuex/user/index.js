import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const defaultState = {
	userToken: localStorage.getItem("t"),
	endpoints: {
		obtainJWT: "http://127.0.0.1:8000/auth/obtain-token",
		refreshJWT: "http://127.0.0.1:8000/auth/refresh-token"
	},
	currentUser: null
};

export default {
	namespaced: true,
	state: defaultState,
	actions,
	getters,
	mutations
};
