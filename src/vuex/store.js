import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
import userModule from "./user";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		user: userModule
	},
	getters
});
